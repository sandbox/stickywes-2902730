# Media Export Normalizers 

Export more useful information about media like video and images.

Include width and height information; provide simple filenames without full URLs; account for image styles.
