<?php

namespace Drupal\media_export_normalizers\Normalizer;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldType\ImageItem;
use Drupal\serialization\Normalizer\EntityReferenceFieldItemNormalizer;

/**
 * Converts the Drupal image field structures to a normalized array.
 *
 * Grabs urls and sizing information for all image styles.
 *
 */
class ImageWithStyleNormalizer extends EntityReferenceFieldItemNormalizer {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var array
   */
  protected $supportedInterfaceOrClass = [ImageItem::class];

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    $attributes = parent::normalize($object, $format, $context);

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    if ($entity = $object->get('entity')->getValue()) {
    }

    $attribute['styles'] = [];

    // Faster than ImageStyle::loadMultiple()
    $image_styles = \Drupal::entityQuery('image_style')
      ->execute();

    $uri = $entity->get('uri')->getValue();
    $uri = reset($uri)['value'];

    foreach($image_styles as $style_name) {
      $style = ImageStyle::load($style_name);
      $style_uri = $style->buildUri($uri);
      $style_url = $style->buildUrl($uri);

      if (!file_exists($style_uri)) {
        $style->createDerivative($uri, $style_uri);
      }

      /* @var \Drupal\Core\Image\Image $image */
      $image = \Drupal::service('image.factory')->get($style_uri);
      $height = $image->getHeight();
      $width = $image->getWidth();
      $attributes['styles'][$style_name] = array('url' => $style_url, 'width' => $width, 'height' => $height);
    }
    
    return $attributes;
  }

}
